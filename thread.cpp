#include <ucontext.h>
#include <queue>
#include <new>
#include <cassert>
#include <iostream>
#include "thread.h"
#include "impl.h"

using namespace std;

unordered_map<thread *, bool> thread::impl::join_map = unordered_map<thread *, bool>();

queue<thread::impl *> thread::impl::ready_queue = queue<thread::impl *>();
queue<thread::impl *> thread::impl::finished_thread = queue<thread::impl *>();

long long thread::impl::thread_count = 0;

void thread_return(void* a);

struct thread_stuff
{
	thread_startfunc_t func;
	void* arg;
	thread::impl* thread_impl_ptr;
};

thread::impl::impl()
{
	thread_id = thread_count++;
	stack = nullptr;
	ucp_ptr = nullptr;
}

thread::impl::~impl()
{}

thread::thread(thread_startfunc_t func, void * arg)
{
	cpu::interrupt_disable();
	assert_interrupts_disabled();

	if(func != nullptr)
	{
		try{
			while(guard.exchange(true)) {} // put everything inside an atomic area
			impl_ptr = new impl;
			ucontext_t* tmp = new ucontext_t;
			getcontext(tmp);

			impl_ptr->ucp_ptr = tmp;
			impl_ptr->thread_ptr = this;
			impl::join_map[this] = false;
			impl_ptr->stack = new char [STACK_SIZE];
			impl_ptr->ucp_ptr->uc_stack.ss_sp = impl_ptr->stack;
			impl_ptr->ucp_ptr->uc_stack.ss_size = STACK_SIZE;
			impl_ptr->ucp_ptr->uc_stack.ss_flags = 0;
			impl_ptr->ucp_ptr->uc_link = nullptr;

			thread_stuff* a = new thread_stuff;
			a->func = func;
			a->arg = arg;
			a->thread_impl_ptr = impl_ptr;

			makecontext(impl_ptr->ucp_ptr, (void (*)()) thread_return, 1, a); // make the context

			impl::ready_queue.push(impl_ptr);

			if ((!cpu::impl::inactive_cpu.empty())) { // everytime anything is pushed onto the ready queue, try waking up the inactive cpus
				cpu::impl::inactive_cpu.front()->interrupt_send();
				cpu::impl::inactive_cpu.pop_front();
			}

			guard.store(false);
		}
		catch(const std::bad_alloc &e) { // if allocation fails, set the guard to false and enable interrupt
			guard.store(false);
			cpu::interrupt_enable();
			assert_interrupts_enabled();
			throw e; // throw the error
		}
	}
	cpu::interrupt_enable();
	assert_interrupts_enabled();
}

thread::~thread()
{
}

void thread::yield()
{
	cpu::interrupt_disable();
	assert_interrupts_disabled();

	while(guard.exchange(true)) {}
	cpu* current_cpu = cpu::self();

	thread::impl *current_thread_impl_ptr = (current_cpu->impl_ptr)->running_thread_impl_ptr;
	impl::ready_queue.push(current_thread_impl_ptr); // put the running thread onto the ready queue

	if ((!cpu::impl::inactive_cpu.empty())) { // wake up potential cpus to run that thread
		cpu::impl::inactive_cpu.front()->interrupt_send();
		cpu::impl::inactive_cpu.pop_front();
	}

	thread::impl *next_thread_impl_ptr = impl::ready_queue.front();
	impl::ready_queue.pop();

	(current_cpu->impl_ptr)->running_thread_impl_ptr = next_thread_impl_ptr;

	swapcontext(current_thread_impl_ptr->ucp_ptr, next_thread_impl_ptr->ucp_ptr); // swap to the next thread on the ready queue
	guard.store(false);
	cpu::interrupt_enable();
	assert_interrupts_enabled();
}

void thread_return(void* a) // embed the user function inside a helper function, the end of which is detectable
{
	thread_stuff* stuff = (thread_stuff*) a;
	guard.store(false);
	cpu::interrupt_enable();
	assert_interrupts_enabled();
	stuff->func(stuff->arg); // run the user code
	cpu::interrupt_disable();
	assert_interrupts_disabled();
	while(guard.exchange(true)) {} // making the following area atomic
	cpu* current_cpu = cpu::self();
	while(!stuff->thread_impl_ptr->waiting_queue.empty())
	{ // until the waiting queue is empty, keep popping the waiting_queue to the ready queue
		thread::impl::ready_queue.push(stuff->thread_impl_ptr->waiting_queue.front());
		stuff->thread_impl_ptr->waiting_queue.pop();
		// wait up inactive cpus at the same time
		if ((!cpu::impl::inactive_cpu.empty())) {
			cpu::impl::inactive_cpu.front()->interrupt_send();
			cpu::impl::inactive_cpu.pop_front();
		}
	}

	thread::impl::finished_thread.push(stuff->thread_impl_ptr); // after finishing, put itself onto the finish queue for cpus to delete

	thread::impl::join_map[stuff->thread_impl_ptr->thread_ptr] = true;
	delete stuff;
	setcontext(current_cpu->impl_ptr->CPU_ptr); // go back to cpu
}

void thread::join()
{
	cpu::interrupt_disable();
	assert_interrupts_disabled();
	while(guard.exchange(true)) {}
	cpu* current_cpu = cpu::self();
	thread::impl *now_running_thread_impl_ptr = current_cpu->impl_ptr->running_thread_impl_ptr; // acquire the currently running thread
	if (!impl::join_map[this]) { // if this thread hasn't finished, then execute the following
		this->impl_ptr->waiting_queue.push(now_running_thread_impl_ptr); // put the current running thread on to the waiting queue
		if(impl::ready_queue.empty()) // if ready queue is empty, go back to cpu immediately
			swapcontext(now_running_thread_impl_ptr->ucp_ptr, current_cpu->impl_ptr->CPU_ptr);
		else // else, go to the next ready thread directly
		{
			thread::impl *next_thread_impl_ptr = impl::ready_queue.front();
			impl::ready_queue.pop();
			current_cpu->impl_ptr->running_thread_impl_ptr = next_thread_impl_ptr;
			swapcontext(now_running_thread_impl_ptr->ucp_ptr, next_thread_impl_ptr->ucp_ptr);
		}
	}
	guard.store(false);
	cpu::interrupt_enable();
	assert_interrupts_enabled();
}
