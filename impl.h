#include <ucontext.h>
#include <queue>
#include <deque>
#include <vector>
#include <cassert>
#include <unordered_map>
#include "mutex.h"
#include "thread.h"

using namespace std;

class thread::impl
{
public:

	static queue<thread::impl *> ready_queue;
	static queue<thread::impl *> finished_thread;
	static unordered_map<thread *, bool> join_map;
	static long long thread_count;
	queue<thread::impl *> waiting_queue;
	thread * thread_ptr;
	long long thread_id;
	char* stack;
	ucontext_t* ucp_ptr;

	impl();
	~impl();

};

class cpu::impl
{
public:
	static int empty_run;
	thread::impl *running_thread_impl_ptr;
	bool if_suspend;
	ucontext_t* CPU_ptr;
	char* stack;
	static int active_cpu_num;
	static deque<cpu *> inactive_cpu;
	impl();
	~impl();

};

class mutex::impl{
public:
	queue<thread::impl *> waiting_queue;
	bool if_busy;
	long long holding_thread_id;

	impl();
	~impl();
};

class cv::impl{
public:

	queue<thread::impl *> waiting_queue;

	impl();
	~impl();
};
