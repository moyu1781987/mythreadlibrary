#include <ucontext.h>
#include <queue>
#include <deque>
#include <iostream>
#include "thread.h"
#include "impl.h"

using namespace std;

int cpu::impl::active_cpu_num = 0;
deque<cpu *> cpu::impl::inactive_cpu = deque<cpu *>();
// int cpu::impl::empty_run = 0;

cpu::impl::impl()
{
	CPU_ptr = new ucontext_t;
	stack = new char [STACK_SIZE];
	if_suspend = false;
}

cpu::impl::~impl()
{
	delete CPU_ptr;
	delete[] stack;
}

void Timer_handler(void);

void IPI_handler(void);

void cpu::init(thread_startfunc_t func, void * arg)
{
	while(guard.exchange(true)) {}
	impl::active_cpu_num++;
	guard.store(false);

	try {
		impl_ptr = new impl;
	}
	catch(const std::bad_alloc &e) {
		cpu::interrupt_enable();
		throw e;
	}
	this->interrupt_vector_table[TIMER] = (interrupt_handler_t)Timer_handler;
	this->interrupt_vector_table[IPI] = (interrupt_handler_t)IPI_handler;
    //initialize context and stack on cpu
	getcontext(impl_ptr->CPU_ptr);
	impl_ptr->CPU_ptr->uc_stack.ss_sp = impl_ptr->stack;
	impl_ptr->CPU_ptr->uc_stack.ss_size = STACK_SIZE;
	impl_ptr->CPU_ptr->uc_stack.ss_flags = 0;
	impl_ptr->CPU_ptr->uc_link = nullptr;
	impl_ptr->running_thread_impl_ptr = nullptr;
	interrupt_enable();
	assert_interrupts_enabled();
	thread t1(func, arg); // create the corresponding thread
	interrupt_disable();
	assert_interrupts_disabled();
	while(guard.exchange(true)) {}
	while(true) // keep looping until stopped by infrastructure
	{
		while(!thread::impl::ready_queue.empty())
		{

			impl_ptr->running_thread_impl_ptr = thread::impl::ready_queue.front();
      thread::impl::ready_queue.pop();

			// if ((!cpu::impl::inactive_cpu.empty())) {
			// 	cpu::impl::inactive_cpu.front()->interrupt_send();
			// 	cpu::impl::inactive_cpu.pop_front();
			// }

			assert_interrupts_disabled(); // disable interrupt before swapcontext
			swapcontext(impl_ptr->CPU_ptr, impl_ptr->running_thread_impl_ptr->ucp_ptr); // make the swap
			assert_interrupts_disabled(); // disable interrupt after swapcontext

			while(!thread::impl::finished_thread.empty())
			{ // delete finished thread everytime a thread is finished
				thread::impl *to_delete = thread::impl::finished_thread.front();
				thread::impl::finished_thread.pop();
				delete to_delete->ucp_ptr;
				delete[] to_delete->stack;
				delete to_delete;
			}

			impl_ptr->running_thread_impl_ptr = nullptr;
		}

		if(thread::impl::ready_queue.empty())
		{
			this->impl_ptr->if_suspend = true;
			impl::active_cpu_num--; // decrease the number of active cpus
			impl::inactive_cpu.push_back(this);
			guard.store(false); // giving up the guard before suspend
			assert_interrupts_disabled();
			interrupt_enable_suspend(); // suspend the cpu while enabling interrupt
			assert_interrupts_disabled();
			while(guard.exchange(true)) {} // retrieve the guard after waking up
			impl::active_cpu_num++; // increment the number of active cpus
		}
	}
}

void Timer_handler(void) {	//changed
	cpu::interrupt_disable();
	assert_interrupts_disabled();
	cpu* current_cpu = cpu::self();
	if (current_cpu->impl_ptr->running_thread_impl_ptr != nullptr) { // if the timer handler is not called inside cpu.init()
		while(guard.exchange(true)) {}
		thread::impl *current_thread_impl_ptr = (current_cpu->impl_ptr)->running_thread_impl_ptr;
		thread::impl::ready_queue.push(current_thread_impl_ptr); // push the running thread into ready queue

		if ((!cpu::impl::inactive_cpu.empty())) { // wake up potential cpus
			cpu::impl::inactive_cpu.front()->interrupt_send();
			cpu::impl::inactive_cpu.pop_front();
		}

		thread::impl *next_thread_impl_ptr = thread::impl::ready_queue.front(); // pop the ready queue
		thread::impl::ready_queue.pop();

		(current_cpu->impl_ptr)->running_thread_impl_ptr = next_thread_impl_ptr;

		swapcontext(current_thread_impl_ptr->ucp_ptr, next_thread_impl_ptr->ucp_ptr); // swap to the next thread
		guard.store(false);
	}
	cpu::interrupt_enable();
	assert_interrupts_enabled();
}

void IPI_handler(void) {//changed
  	cpu::interrupt_disable();
	assert_interrupts_disabled();
  	cpu *my_cpu = cpu::self();
  	if (my_cpu->impl_ptr->if_suspend) { // if waked up
   		my_cpu->impl_ptr->if_suspend = false; // set the flag, return to cpu.init()
  	}
  	else { // if called inside executions of other functions, do the same thing as Timer handler does
			cpu* current_cpu = cpu::self();
			if (current_cpu->impl_ptr->running_thread_impl_ptr != nullptr) {
				while(guard.exchange(true)) {}

				thread::impl *current_thread_impl_ptr = (current_cpu->impl_ptr)->running_thread_impl_ptr;
				thread::impl::ready_queue.push(current_thread_impl_ptr);

				if ((!cpu::impl::inactive_cpu.empty())) {
					cpu::impl::inactive_cpu.front()->interrupt_send();
					cpu::impl::inactive_cpu.pop_front();
				}

				thread::impl *next_thread_impl_ptr = thread::impl::ready_queue.front();
				thread::impl::ready_queue.pop();

				(current_cpu->impl_ptr)->running_thread_impl_ptr = next_thread_impl_ptr;

				swapcontext(current_thread_impl_ptr->ucp_ptr, next_thread_impl_ptr->ucp_ptr);
				guard.store(false);
			}
		cpu::interrupt_enable();
		assert_interrupts_enabled();
  	}
}
