#include <ucontext.h>
#include <queue>
#include <stdexcept>
#include <iostream>
#include "mutex.h"
#include "impl.h"
#include "thread.h"

using namespace std;

mutex::mutex():impl_ptr(new impl){
}

mutex::~mutex(){
  delete impl_ptr;
}

mutex::impl::impl():if_busy(false), holding_thread_id(-1){
  waiting_queue = queue<thread::impl *>();
}

mutex::impl::~impl(){}

void mutex::lock()
{
  cpu::interrupt_disable();
  assert_interrupts_disabled();
  while(guard.exchange(true)) {}
  if(this->impl_ptr->if_busy) // check if lock is busy
  {
    // if lock is busy, put the calling thread onto the waiting queue and swap back to cpu
    thread::impl *cur_thread_impl = cpu::self()->impl_ptr->running_thread_impl_ptr;
    this->impl_ptr->waiting_queue.push(cur_thread_impl);
    swapcontext(cur_thread_impl->ucp_ptr,cpu::self()->impl_ptr->CPU_ptr);
  }
  this->impl_ptr->if_busy = true; // grab the lock
  this->impl_ptr->holding_thread_id = cpu::self()->impl_ptr->running_thread_impl_ptr->thread_id; // change the holding thread id accordingly
  guard.store(false);
  cpu::interrupt_enable();
  assert_interrupts_enabled();
}

void mutex::unlock()
{
  cpu::interrupt_disable();
  assert_interrupts_disabled();

  while(guard.exchange(true)) {}

  if (this->impl_ptr->holding_thread_id == cpu::self()->impl_ptr->running_thread_impl_ptr->thread_id) { // if the holding thread matches with the calling thread
    this->impl_ptr->if_busy = false;
    this->impl_ptr->holding_thread_id = -1;

    if(!this->impl_ptr->waiting_queue.empty()) //if the waiting_queue is not empty, hand off the lock to the next one in line
    {
      thread::impl *temp;
      temp = this->impl_ptr->waiting_queue.front();
      this->impl_ptr->waiting_queue.pop();
      thread::impl::ready_queue.push(temp);
      this->impl_ptr->holding_thread_id = temp->thread_id;
      this->impl_ptr->if_busy = true;

      if ((!cpu::impl::inactive_cpu.empty())) { // wake up potential suspended cpus
				cpu::impl::inactive_cpu.front()->interrupt_send();
				cpu::impl::inactive_cpu.pop_front();
			}

    }
  }
  else { // if the holding thread doesn't matching with the calling thread, throw an error
    guard.store(false);
    cpu::interrupt_enable();
    assert_interrupts_enabled();
    throw std::runtime_error("Unlock mutex without acquiring it");
  }
  guard.store(false);
  // cout << "unlock enable\n";
  cpu::interrupt_enable();
  assert_interrupts_enabled();
}
