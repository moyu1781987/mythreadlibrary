#include <ucontext.h>
#include <queue>
#include <iostream>
#include "impl.h"
#include "thread.h"

using namespace std;

cv::cv(){
  impl_ptr = new impl;
}

cv::~cv(){
  delete impl_ptr;
}

cv::impl::impl() {}

cv::impl::~impl(){}

void cv::wait(mutex& t1)
{
  // release the lock
  cpu::interrupt_disable();
  assert_interrupts_disabled();
  while(guard.exchange(true)) {}
  if (t1.impl_ptr->holding_thread_id == cpu::self()->impl_ptr->running_thread_impl_ptr->thread_id) { // if the holding thread matches with the calling thread
    t1.impl_ptr->if_busy = false;
    t1.impl_ptr->holding_thread_id = -1;

    if(!t1.impl_ptr->waiting_queue.empty()) //if the waiting_queue is not empty, hand off the lock to the next one in line
    {
      thread::impl *temp;
      temp = t1.impl_ptr->waiting_queue.front();
      t1.impl_ptr->waiting_queue.pop();
      thread::impl::ready_queue.push(temp);
      t1.impl_ptr->holding_thread_id = temp->thread_id;
      t1.impl_ptr->if_busy = true;

      if ((!cpu::impl::inactive_cpu.empty())) { // wake up potential suspended cpus
				cpu::impl::inactive_cpu.front()->interrupt_send();
				cpu::impl::inactive_cpu.pop_front();
			}

    }
  }
  else { // if the holding thread doesn't matching with the calling thread, throw an error
    guard.store(false);
    cpu::interrupt_enable();
    assert_interrupts_enabled();
    throw std::runtime_error("Unlock mutex without acquiring it");
  }

  thread::impl *temp = cpu::self()->impl_ptr->running_thread_impl_ptr;
  this->impl_ptr->waiting_queue.push(temp); // put current context onto waiting queue

  swapcontext(temp->ucp_ptr,cpu::self()->impl_ptr->CPU_ptr); // go to sleep

  if(t1.impl_ptr->if_busy) // waking up, check if the lock is busy
  {
    thread::impl *cur_thread_impl = cpu::self()->impl_ptr->running_thread_impl_ptr;
    t1.impl_ptr->waiting_queue.push(cur_thread_impl); //if busy, add current context to waiting queue & switch back to cpu to handle next thread
    swapcontext(cur_thread_impl->ucp_ptr,cpu::self()->impl_ptr->CPU_ptr);
  }

  t1.impl_ptr->if_busy = true; // grab the lock, waking up
  t1.impl_ptr->holding_thread_id = cpu::self()->impl_ptr->running_thread_impl_ptr->thread_id;
  guard.store(false);
  cpu::interrupt_enable();
  assert_interrupts_enabled();
}

void cv::signal()
{
  cpu::interrupt_disable();
  assert_interrupts_disabled();

  while(guard.exchange(true)) {}
  if (!this->impl_ptr->waiting_queue.empty()) //if waiting queue is not empty
  {
    thread::impl *next = this->impl_ptr->waiting_queue.front(); // wake up one thread
    this->impl_ptr->waiting_queue.pop();
    thread::impl::ready_queue.push(next); // push the thread onto ready queue

    if ((!cpu::impl::inactive_cpu.empty())) { // wake up all inactive cpus immediately
      cpu::impl::inactive_cpu.front()->interrupt_send();
      cpu::impl::inactive_cpu.pop_front();
    }
  }

  guard.store(false);
  cpu::interrupt_enable();
  assert_interrupts_enabled();
}

void cv::broadcast()
{
  cpu::interrupt_disable();
  assert_interrupts_disabled();

  while(guard.exchange(true)) {}   //if waiting queue is not empty
  while(!this->impl_ptr->waiting_queue.empty()) // while the waiting queue is not empty
  {
    thread::impl *tmp = this->impl_ptr->waiting_queue.front(); // wake up all threads
    this->impl_ptr->waiting_queue.pop();
    thread::impl::ready_queue.push(tmp);

    if ((!cpu::impl::inactive_cpu.empty())) { // wake up all inactive cpus immediately
      cpu::impl::inactive_cpu.front()->interrupt_send();
      cpu::impl::inactive_cpu.pop_front();
    }
  }
  guard.store(false); // release the guard
  cpu::interrupt_enable();
  assert_interrupts_enabled();
}
